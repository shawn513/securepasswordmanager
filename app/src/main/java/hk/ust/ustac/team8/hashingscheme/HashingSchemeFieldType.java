package hk.ust.ustac.team8.hashingscheme;

/**
 * Created by logchan on 1/6/2015.
 */
public enum HashingSchemeFieldType {
    STRING,
    EMAIL,
    NUMBER
}
