package hk.ust.ustac.team8.hashingscheme;

/**
 * Created by logchan on 1/6/2015.
 */
public enum HashingSchemeTransform {
    NO_TRANSFORM,
    MIXED_UPPER_AND_LOWER_CASE
}
